import { LightningElement, api, track } from 'lwc';
import { checkValability }  from 'c/checkAvailability';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class FormContact extends LightningElement {
    @api contactinfo; // id: ...
    @track recordId; // for the template, set the id of current contact

    @track contact = {
        AccountId: "",
        Id: "",
        FirstName: "",
        LastName: "",
        Birthdate: "",
        Phone: "",
        Email: "",
        Fiscal_Code__c: "",
    }

    // called when the component is inserted in document
    connectedCallback() {
        this.editContact();
    }

    deleteContact() {
        console.log("delete 1");
        // ci dobbiamo creare un nostro evento custom (nomeEvento, {detail: data to send})
        const onRemoveContact = new CustomEvent('contactremove', { detail: { ind: this.contactinfo.Id } });
        // facciamo scattare l'evento, per fare questo si usa dispatchEvent()
        this.dispatchEvent(onRemoveContact);
    }
    @api
    getFields(){
        let clonedContact = {...this.contact};
        try{
            const fields = this.template.querySelector("[data-id='form']");
            fields.querySelectorAll("lightning-input-field").forEach(obj => {
                if(obj.fieldName === 'Fiscal_Code__c'){
                    clonedContact[obj.fieldName] = obj.value.toUpperCase();
                } else {
                    clonedContact[obj.fieldName] = obj.value;
                }
            });
            checkValability(clonedContact, clonedContact.Fiscal_Code__c);
        } catch(error){
            const event = new ShowToastEvent({
                title: 'Error',
                message: error,
                variant: 'error',
            });
            this.dispatchEvent(event);
            throw 'error';
        }
        console.log(JSON.stringify(clonedContact)); 
        return clonedContact;
    }
    /*  @api
      validate() {
          const arr = [];
          this.template.querySelectorAll(".validate").forEach(obj => {
              console.log(obj.fieldName);
              console.log(obj.value);
              if (obj.fieldName === "Fiscal_Code__c") {
                  if (this.validateCF(obj.value) != undefined) {
                      arr.push(obj.value);
                  }
              } else if (obj.value === '') {
                  arr.push(obj.fieldName); // ritorna un array con i nomi dei field non completati
              }
          });
          return arr;
      } */
    @api
    validate() {
        const val = [];
        this.template.querySelectorAll(".validate").forEach(obj => {
            if(obj.value === '') {
                val.push(obj.fieldName);
            }
        });
        return val;
    }

    // Scope: It's used to set the id of a contact
    @api
    setContactId(param) {
        console.log("param " + JSON.stringify(param));
        this.recordId = param.id;
        this.contact.Id = param.id;
        this.contact.AccountId = param.accountId;
        console.log("Contact " + JSON.stringify(this.contact));
    }

    @api
    editContact() {
        //console.log('contact in formContact edit ' + contact);
        console.log(JSON.stringify("formContact js " + this.contactinfo));
        if (this.contactinfo !== "") {
            this.contact = this.contactinfo;
            console.log(JSON.stringify(this.contact));
        }
    }

    /*  validateCF(param) {
          var re = /^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i;
          if (param != '') {
              var OK = re.exec(param);
              if (!OK) {
                  console.log('no');
                  return "Fiscal_Code__c";
              }
           //  /* else {
           //       console.log('si');
           //   }
          } else {
              return "Fiscal_Code__c";
          }
      }  */


    /* validaCodiceFiscale(cf) {
         var validi, i, s, set1, set2, setpari, setdisp;
         if (cf == '') return false;
         cf = cf.toUpperCase();
         if (cf.length != 16)
             return false;
         validi = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
         for (i = 0; i < 16; i++) {
             if (validi.indexOf(cf.charAt(i)) == -1)
                 return false;
         }
         set1 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         set2 = "ABCDEFGHIJABCDEFGHIJKLMNOPQRSTUVWXYZ";
         setpari = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         setdisp = "BAKPLCQDREVOSFTGUHMINJWZYX";
         s = 0;
         for (i = 1; i <= 13; i += 2)
             s += setpari.indexOf(set2.charAt(set1.indexOf(cf.charAt(i))));
         for (i = 0; i <= 14; i += 2)
             s += setdisp.indexOf(set2.charAt(set1.indexOf(cf.charAt(i))));
         if (s % 26 != cf.charCodeAt(15) - 'A'.charCodeAt(0))
             return false;
         return true;
     }*/



}