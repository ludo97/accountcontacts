({
    init: function (component, event, helper) {
        // return all account for start app
        helper.getAllAccounts(component, event, helper);
    },
    getAccountId: function (component, event, helper) {
        var accoundId = event.getParam('ind');
        var navService = component.find("navService");
        var pageRef = {
            type: 'standard__component',
            attributes: {
                componentName: 'c__ManageForms'
            },
            state: {
                c__recordId: accoundId
            }
        };
        navService.navigate(pageRef);
    },
    checkInputData: function (component, event, helper) {
        var eventInput = event.getSource();
        var dataToSend = component.get("v.data");
        var nameOfEvent = eventInput.get("v.name");
        var valueOfEvent = eventInput.get("v.value");
        var data = {
            Name: '',
            BillingCountry: ''
        };
        (dataToSend != null) ? data = dataToSend : '';
        if (valueOfEvent.length >= 2) {
            (nameOfEvent === "name") ? data.Name = valueOfEvent : data.BillingCountry = valueOfEvent;
        } else {
            (nameOfEvent === "name") ? data.Name = '' : data.BillingCountry = '';
        }
        component.set("v.data", data);
        helper.sendData(component, event, helper);
    },
})
