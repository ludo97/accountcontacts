public with sharing class AccountRepository {
	public AccountRepository() {
	}

	public Account findById(String accId) {
		return [
			SELECT
				Id,
				Name,
				Phone,
				Website,
				BillingStreet,
				BillingCity,
				BillingCountry
			FROM Account
			WHERE Id = :accId
		];
	}

	/* if( name !='' && country !=''){  
    query += ' WHERE Name LIKE \'%' + name + '%\' AND BillingCountry LIKE \'%' + country + '%\'';
    }
    else{
        if( name !=''){
      query += ' WHERE Name LIKE \'%' + name + '%\'';
        }
        else if(country !=''){
      query += ' WHERE BillingCountry LIKE \'%' + country + '%\'';
        }
    } */

	public List<Account> getAccountsByCondition(
		List<String> fields,
		Map<String, String> conditions
	) {
		String query = 'SELECT ';
		for (Integer i = 0; i < fields.size(); i++) {
			query += (i < fields.size() - 1)
				? fields.get(i) + ', '
				: fields.get(i) + ' FROM Account WHERE ';
		}
		query += getQuerySelect(conditions);
		return Database.query(query);
	}

	private String getQuerySelect(Map<String, String> ac) {
		String query = '';
		Integer i = 0;
		for (String key : ac.keySet()) {
			query += (String.isEmpty(ac.get(key)))
				? key + '!=\'Null\''
				: key + ' LIKE \'' + ac.get(key) + '%\'';
			if (i + 1 < ac.size()) {
				query += ' AND ';
			}
			i++;
		}
		return query;
	}
	public List<Account> getAllAccounts() {
		return [SELECT Id, Name, Phone, Website, BillingCountry FROM Account];
	}

	public Account saveAccount(Account account) {
		insert account;
		return account;
	}

	public Account updateAccount(Account account) {
		update account;
		return account;
	}

	public void removeAccount(Account account) {
		delete account;
	}
}
