// Controller
public with sharing class AccountManagerController {
    public AccountManagerController() {}

    private static AccountService accountService = new AccountService();
    private static ContactService contactService = new ContactService();
    private static ResponseDTO responseDto = new ResponseDTO();

    @AuraEnabled
    public static ResponseDTO sendAll(Map<String, String> param){ // json con account e contact
        try {
            // upsert = insert/update
            responseDto.content = accountService.manageAccountContact(param);
            responseDto.status = true;
        } catch (Exception ex){
            responseDto.errorMessage = (ex.getMessage());
            responseDto.errorCode = (ex.getStackTraceString());
        }
        return responseDto;
     }

     // this method is used for retrive all accounts that maches the name and country criteria
     @AuraEnabled
     public static ResponseDTO getAccounts(Map<String, String> param){
         try {
            System.debug('Hi');
            responseDto.content = accountService.getAccountsService(param);
            System.debug('Hi ' + responseDto.content);
            responseDto.status = true;
         } catch (Exception ex){
            System.debug('Not working');
            responseDto.errorMessage = (ex.getMessage());
            responseDto.errorCode = (ex.getStackTraceString());
        }
        return responseDto;
     }

     @AuraEnabled
     public static ResponseDTO getAllAccounts(){
        try {
            responseDto.content = accountService.getAllAccount();
            System.debug('Hi ' + responseDto.content);
            responseDto.status = true;
         } catch (Exception ex){
            System.debug('Not working');
            responseDto.errorMessage = (ex.getMessage());
            responseDto.errorCode = (ex.getStackTraceString());
        }
        return responseDto;
     }

     @AuraEnabled
     public static ResponseDTO getId(String accId){
      try{
         responseDto.content = accountService.getAccountById(accId);
         responseDto.status = true;
      } catch(Exception e){
         responseDto.errorMessage = e.getMessage();
         responseDto.errorCode = e.getStackTraceString();
      }
         System.debug('response: ' + responseDto);
         return responseDto;
      }
   }
