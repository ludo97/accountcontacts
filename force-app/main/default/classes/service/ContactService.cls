public with sharing class ContactService {
    public ContactService() {}

    private ContactRepository contactRepository = new ContactRepository();

    public List<String> insertOrUpdateContact(List<Contact> contacts, Account acc){
        List<String> listId = new List<String>();
        String prova = 'fit';
        for(Contact c : contacts){
            String idContact = c.Id;
            if (idContact.contains(prova)) {  
         /*   }
            if(String.isEmpty(c.Id)){ */
                addContact(c, acc.Id);
            } else{
                updateContact(c);
            }
            listId.add(c.Id);
        }
        return listId;
    }
        
    public List<Contact> manageContact(Map<String, String> param, Account acc){
        List<Contact> listCnt = new List<Contact>();
        List<Contact> listContactsJson = (List<Contact>) JSON.deserialize(param.get('contact'), List<Contact>.class);
        List<String> listContactsId = insertOrUpdateContact(listContactsJson, acc);
        List<Contact> listContactFromDb = findContactByAccountId(acc.Id);
        if(listContactsJson.size() > 0){
            for(Contact cnt : listContactFromDb){
                if(listContactsId.contains(cnt.Id)){
                    listCnt.add(cnt);
                } else {
                    removeContact(cnt);
                }
            }
        } else {
            removeContact(listContactFromDb);
        }
        return listCnt;
    }

    public List<Contact> addContact(List<Contact> contactList, String accountId) {
        List<Contact> lc = new List<Contact>();
        for (Contact c : contactList) {
            c.AccountId = accountId;
            lc.add(c);
        }
        return addContact(lc);
    }

    public List<Contact> addContact(List<Contact> contactList) {
        List<Contact> lc = new List<Contact>();
        for (Contact c : contactList) {
            lc.add(addContact(c));
        }
        return lc;
    }

    public Contact addContact(Contact contact, String accountId) {
        contact.Id = null;
        contact.AccountId = accountId;
        return addContact(contact);
    }

    public Contact addContact(Contact contact) {
        Contact contactRepo = contactRepository.saveContact(contact);
        return contactRepo;
    }

    public List<Contact> updateContact(List<Contact> contactList) {
        List<Contact> lc = new List<Contact>();
        for (Contact c : contactList) {
            lc.add(updateContact(c));
        }
        return lc;
    }

    public Contact updateContact(Contact contact) {
        Contact contactRepo = contactRepository.updateContact(contact);
        return contactRepo;
    }


    public void removeContact(List<Contact> contactList) {
        for (Contact c : contactList) {
            removeContact(c);
        }
    }

    public void removeContact(Contact contact) {
        contactRepository.removeContact(contact);
    }


    public List<Contact> findContactByAccountId(String accountId) {
        return contactRepository.findContactByAccountId(accountId);
    }
}
